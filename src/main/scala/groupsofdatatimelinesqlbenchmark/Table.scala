package groupsofdatatimelinesqlbenchmark

import java.sql.Timestamp

import slick.jdbc.GetResult
import slick.jdbc.PostgresProfile.api._

case class Encounter(encid: Long, starttime: Timestamp, zone: String)

class Encounters(tag: Tag) extends Table[Encounter](tag, "encounter") {

  def encid = column[Long]("encid", O.PrimaryKey, O.AutoInc)

  def starttime = column[Timestamp]("starttime")

  def zone = column[String]("zone")

  def * = (encid, starttime, zone) <> (Encounter.tupled, Encounter.unapply)

}

object Tables {

  val Encounters = new TableQuery(tag => new Encounters(tag))
  implicit val getEncountersResult: GetResult[Encounter] = GetResult(r => Encounter(r.<<, r.<<, r.<<))

}
