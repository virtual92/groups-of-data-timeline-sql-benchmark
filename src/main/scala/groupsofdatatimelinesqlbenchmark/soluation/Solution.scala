package groupsofdatatimelinesqlbenchmark.soluation

import groupsofdatatimelinesqlbenchmark.{Encounter, GlobalSolutionChecker}

trait Solution {

  protected def query(groups: Int): List[Encounter]

  def solve(groups: Int) = {
    val encounters = query(groups)
    GlobalSolutionChecker.validateNext(encounters.toSet)
  }

  def name(): String

}
