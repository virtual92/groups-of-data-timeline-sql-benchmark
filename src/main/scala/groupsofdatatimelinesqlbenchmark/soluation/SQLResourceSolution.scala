package groupsofdatatimelinesqlbenchmark.soluation

import groupsofdatatimelinesqlbenchmark.{DB, Encounter, Tables}
import org.apache.commons.io.IOUtils
import slick.jdbc.GetResult
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.Await
import concurrent.duration._

class SQLResourceSolution(resourcePath: String) extends Solution {

  private val templateSql = IOUtils.toString(getClass.getClassLoader.getResourceAsStream(resourcePath), "utf-8")
  private implicit val getEncountersResult: GetResult[Encounter] = Tables.getEncountersResult

  override def query(groups: Int): List[Encounter] = {
    val finalSql = templateSql.replace("${groups}", groups.toString)
    val res = Await.result(DB().run(sql"#$finalSql".as[Encounter]), 999 minutes)
    res.toList
  }

  override val name: String = resourcePath
}
