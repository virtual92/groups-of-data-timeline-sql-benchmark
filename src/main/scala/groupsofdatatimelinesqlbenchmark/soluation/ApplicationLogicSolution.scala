package groupsofdatatimelinesqlbenchmark.soluation

import groupsofdatatimelinesqlbenchmark.{DB, Encounter, Tables}

import scala.annotation.tailrec
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.Await
import concurrent.duration._

class ApplicationLogicSolution extends Solution {

  private def getRecentData(offset: Int, limit: Int): Seq[Encounter] = {
    Await.result(DB().run(Tables.Encounters.drop(offset).take(limit).result), 10 minutes)
  }

  type EncounterGrouped = List[(String, List[Encounter])]

  type Fetch[T] = (Int, Int) => T
  type Combine[T] = (T, T) => T
  type Break[T] = T => Boolean

  def foldWithBreakAndFetch[T](fetch: Fetch[T], combine: Combine[T], break: Break[T]): T = {
    val bulk = 20

    @tailrec
    def iter(acc: T, offset: Int): T = {
      if (break(acc)) acc
      else {
        val fetched = fetch(offset, bulk)
        val combined = combine(acc, fetched)
        iter(combined, offset + bulk)
      }
    }

    iter(fetch(0, bulk), bulk)
  }

  def getRecentGrupedEncounters(size: Int): EncounterGrouped = {
    val fetch: Fetch[EncounterGrouped] = (offset, limit) => {
      val encounters = getRecentData(offset, limit)

      def iter(acc: EncounterGrouped, remaining: List[Encounter]): EncounterGrouped = {
        if (remaining.isEmpty) acc
        else {
          val (head :: tail) = remaining
          val newAcc = if (acc.isEmpty) {
            List(head.zone -> List(head))
          } else if (acc.head._2.head.zone == head.zone) {
            acc.head._1 -> (head :: acc.head._2) :: acc.tail
          } else {
            head.zone -> List(head) :: acc
          }
          iter(newAcc, tail)
        }
      }

      iter(Nil, encounters.toList)
    }

    val combine: Combine[EncounterGrouped] = (acc, next) => {
      if (next.isEmpty) acc else {
        val (acchead :: acctail) = acc
        val (nextrhead :: nextrtail) = next.reverse

        if (acchead._2.head.zone == nextrhead._2.head.zone) {
          val combined = acchead._2.head.zone -> (acchead._2 ::: nextrhead._2)
          nextrtail.reverse ::: combined :: acctail
        } else {
          next ::: acc
        }
      }
    }

    val break: Break[EncounterGrouped] = encGroup => encGroup.size > size

    val encounterGrouped = foldWithBreakAndFetch(fetch, combine, break)

    encounterGrouped.reverse.map(e => e._1 -> e._2.reverse).take(size)
  }

  override def query(groups: Int): List[Encounter] = {
    getRecentGrupedEncounters(groups)
      .flatMap(_._2)
  }

  override def name() = "ApplicationSolution"
}
