package groupsofdatatimelinesqlbenchmark

import scala.concurrent.duration.Duration

case class BenchmarkEntry(method: String, entries: Int, time: Duration) {

}
