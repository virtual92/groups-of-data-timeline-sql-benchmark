package groupsofdatatimelinesqlbenchmark

import slick.jdbc.PostgresProfile
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.ExecutionContext.Implicits.global

object DB {

  private val db = Database.forConfig("db")

  def apply(): PostgresProfile.backend.Database = db

}
