package groupsofdatatimelinesqlbenchmark

import java.util.Objects

import com.typesafe.scalalogging.LazyLogging

object GlobalSolutionChecker extends LazyLogging {

  var first: Set[Encounter] = _

  def validateNext(encounters: Set[Encounter]): Unit = {
    val zeroedEncounters = encounters.map(_.copy(encid = 0))
    if (Objects.isNull(first)) {
      first = zeroedEncounters
    } else {
      if (first != zeroedEncounters) {
        logger.error(s"Not same encounters:\ndifference 1:\n${(first -- zeroedEncounters).mkString("\n")}\ndifference 2:\n${(zeroedEncounters -- first).mkString("\n")}" +
          s"\ncount of first ${first.size}\ncount of second ${zeroedEncounters.size}")

        throw new IllegalStateException()
      }
    }
  }

}
