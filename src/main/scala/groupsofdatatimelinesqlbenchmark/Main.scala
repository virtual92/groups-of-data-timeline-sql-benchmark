package groupsofdatatimelinesqlbenchmark

import com.typesafe.scalalogging.LazyLogging
import groupsofdatatimelinesqlbenchmark.soluation.{ApplicationLogicSolution, SQLResourceSolution, Solution}

object Main extends LazyLogging {

  val rowsOfData = List(100, 1000, 5000, 10000 , 25000, 50000, 100000)
  val solutions: List[Solution] = List(
    new SQLResourceSolution("solution1.sql"),
    new SQLResourceSolution("solution2.sql"),
    new ApplicationLogicSolution
  )
  val latestNGroups = 4


  def main(args: Array[String]): Unit = {

    ResetInsert.init(rowsOfData.max)

    val results =
      rowsOfData.flatMap(count => {
        ResetInsert(count)
        solutions.map(solution => {
          val duration = Benchmark.timed(solution.name() + s"with $count rows", () => {
            solution.solve(latestNGroups)
          })
          BenchmarkEntry(solution.name(), count, duration)
        })
      })

    val grouped = results.groupBy(_.method)

    val printable = grouped.values.map(result => {
      val data = result.map(benchmark => s"${benchmark.entries},${benchmark.time.toMillis}").mkString("\n")
      result.head.method -> data
    })

    val toConsole = printable.map(e => s"${e._1}\ncount,millis\n${e._2}").mkString("\n\n")
    println(toConsole)


  }
}
