package groupsofdatatimelinesqlbenchmark

import java.time.Instant

import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.duration.{Duration, FiniteDuration}
import concurrent.duration._

object Benchmark extends LazyLogging {

  def timed(name: String, action: () => Unit): FiniteDuration = {
    val now = Instant.now()
    action()
    val duration = (Instant.now().toEpochMilli - now.toEpochMilli) millis;
    logger.info(s"$name took ${duration.toMillis} millis")
    duration
  }


}
