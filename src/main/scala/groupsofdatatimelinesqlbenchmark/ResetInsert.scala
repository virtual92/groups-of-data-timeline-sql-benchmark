package groupsofdatatimelinesqlbenchmark

import java.sql.Timestamp
import java.time.Instant

import com.typesafe.scalalogging.LazyLogging
import slick.jdbc.PostgresProfile.api._

import scala.collection.mutable.ArrayBuffer
import scala.concurrent.Await
import scala.util.{Failure, Random, Success, Try}
import scala.concurrent.duration._

object ResetInsert extends LazyLogging {

  private val randomSeed = 5345325
  private val zoneList = ArrayBuffer("A", "B", "C", "D")
  private var data: List[Encounter] = _
  private val date = Instant.ofEpochMilli(1509905271356l)

  def apply(rows: Int) = {

    Try {
      Await.result(DB().run(Tables.Encounters.schema.create), 10 seconds)
    } match {
      case Failure(exception) => logger.trace(s"Couldn't create:  ${exception.getMessage}")
      case Success(_) =>
    }

    dropIndex()
    Await.result(DB().run(sql"""TRUNCATE TABLE encounter""".as[String]), 999 minutes)
    createIndex()
    Benchmark.timed(s"Generating and inserting $rows of data", () => {
      Await.result(DB().run(Tables.Encounters ++= data.reverse.take(rows)), 999 minutes)
    })

  }

  def init(max: Int) = {
    data = getEncounterData(max)
  }

  def createIndex(): Unit = {
    Try {
      val value = sql"""CREATE INDEX encounter_starttime_inndex ON encounter(starttime)""".as[String]
      Await.result(DB().run(value), 10 minutes)
    } match {
      case Failure(exception) => logger.error("Error while creating index", exception)
      case Success(_) =>
    }
  }

  def dropIndex(): Unit = {
    Try {
      val value = sql"""DROP INDEX encounter_starttime_inndex CASCADE""".as[String]
      Await.result(DB().run(value), 10 minutes)
    } match {
      case Failure(exception) => logger.error("Error while dropping index", exception)
      case Success(_) =>
    }

  }

  private def getEncounterData(count: Int) = {
    val random = new Random(randomSeed)

    def getNextGroupSize = 10 + random.nextInt(10)

    def getNextZone(lastZone: String) = (zoneList - lastZone) (random.nextInt(zoneList.length - 1))

    def iter(acc: List[Encounter], n: Int): List[Encounter] = {
      if (acc.size >= count) acc.reverse.take(count)
      else {
        val zone = getNextZone(acc.headOption.map(_.zone).getOrElse(""))
        val nextElems = (1 to getNextGroupSize).map(_ + n)
          .map(x => Encounter(0, Timestamp.from(date.minusSeconds(x)), zone)).toList

        iter(nextElems ::: acc, n + nextElems.size)
      }
    }

    iter(List(), 0).reverse
  }

  def main(args: Array[String]): Unit = {
    println(getEncounterData(30).mkString("\n"))
  }

}
