SELECT *
FROM encounter
ORDER BY starttime DESC
LIMIT (SELECT sum(count)
       FROM (
              WITH magic AS
              (SELECT
                 e.*,
                 min(COALESCE(e1.starttime, now())) AS groupid
               FROM encounter e LEFT JOIN encounter e1 ON e1.starttime > e.starttime AND e1.zone != e.zone
               GROUP BY e.encid
               ORDER BY groupid DESC)
              SELECT
                count(groupid),
                groupid
              FROM magic
              GROUP BY groupid
              ORDER BY groupid DESC
              LIMIT ${groups}
            ) a)