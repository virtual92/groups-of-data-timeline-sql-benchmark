with t1 as (
    select e.*
      -- Detect the zones leading edges
      , case when zone = lag(zone) over (order by starttime desc)
      then 0 -- Same zone as previous
        else 1 -- Found a leading edge
        end edge
    from encounter e
), t2 as (
    select t1.*
      -- Turn the edges into groups
      , sum(edge) over (order by starttime desc rows between unbounded preceding and current row) grp
    from t1
)
select * from t2
where grp <= ${groups};