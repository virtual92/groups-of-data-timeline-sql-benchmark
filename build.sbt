name := "groups-of-data-timeline-sql-benchmark"

version := "0.1"

scalaVersion := "2.12.3"

libraryDependencies ++= Seq(
  "com.typesafe" % "config" % "1.3.1",
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.7.2",
  "commons-io" % "commons-io" % "2.5",
  "org.apache.commons" % "commons-lang3" % "3.6",
  "com.impossibl.pgjdbc-ng" % "pgjdbc-ng" % "0.7.1",
  "com.zaxxer" % "HikariCP" % "2.7.2",
  "com.typesafe.slick" %% "slick" % "3.2.1"
)

javaOptions in Universal ++= Seq(
  "-J-Xmx512m",
  "-J-Xms512m"
)

mainClass in Compile := Some("groupsofdatatimelinesqlbenchmark.Main")

enablePlugins(JavaAppPackaging)


